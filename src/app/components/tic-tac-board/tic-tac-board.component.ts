import { Component } from '@angular/core';
import { TicTacService } from '../../services/tic-tac.service';

@Component({
  selector: 'tic-tac-board',
  templateUrl: './tic-tac-board.component.html',
  styleUrls: ['./tic-tac-board.component.css']
})
export class TicTacBoardComponent {
  board;
  status: string;
  newGameAvailable: boolean;

  constructor(public ticTacService: TicTacService){
    this.board = this.ticTacService.currentBoard;
  }

  ngOnInit() {
    this.ticTacService.newGameAvailable$.subscribe(data => this.changeNewGameAvailable(data));
    this.ticTacService.status$.subscribe(data => this.changeStatus(data));
  }

  handleOnCellClicked(cellIndex){
    this.ticTacService.onCellClicked(cellIndex)
  }

  newGameButtonClicked(){
    this.ticTacService.resetGame()
  }

  changeStatus(data){
    this.status = data;
  }

  changeNewGameAvailable(data){
    this.newGameAvailable = data;
  }

}
