import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'tic-tac-cell',
  templateUrl: './tic-tac-cell.component.html',
  styleUrls: ['./tic-tac-cell.component.css']
})
export class TicTacCellComponent {
  @Input() cell;
  @Input() newGameAvailable: boolean;
  @Output() onCellClicked = new EventEmitter();

  cellClicked(event){
        if(this.cell.value == "" && !this.newGameAvailable){
          this.onCellClicked.emit(this.cell.index);
        }
    }
}
