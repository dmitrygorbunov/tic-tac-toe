/* Angular modules */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MdButtonModule } from '@angular/material';

/* Work modules */
import { AppComponent } from './app.component';
import { TicTacBoardComponent } from './components/tic-tac-board/tic-tac-board.component';
import { TicTacCellComponent } from './components/tic-tac-board/tic-tac-cell/tic-tac-cell.component';
import { TicTacService } from './services/tic-tac.service';

@NgModule({
  declarations: [
    AppComponent,
    TicTacBoardComponent,
    TicTacCellComponent
  ],
  imports: [
    BrowserModule,
    MdButtonModule
  ],
  providers: [TicTacService],
  bootstrap: [AppComponent]
})
export class AppModule { }
