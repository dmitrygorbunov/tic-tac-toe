import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class TicTacService {
  filledCells: number = 0;
  currentPlayer: string = "X";
  newGameAvailable: boolean = false;
  status: string = "";
  currentBoard = {
    rows: [
      {
        cells: [
          {
            index: "1",
            value: ""
          },
          {
            index: "2",
            value: ""
          },
          {
            index: "3",
            value: ""
          }
        ]
      },
      {
        cells: [
          {
            index: "4",
            value: ""
          },
          {
            index: "5",
            value: ""
          },
          {
            index: "6",
            value: ""
          }
        ]
      },
      {
        cells: [
          {
            index: "7",
            value: ""
          },
          {
            index: "8",
            value: ""
          },
          {
            index: "9",
            value: ""
          }
        ]
      }
    ]
  }

  newGameAvailable$: EventEmitter<any>;
  status$: EventEmitter<any>;

  constructor() {
        this.newGameAvailable$ = new EventEmitter();
        this.status$ = new EventEmitter();
    }

  onCellClicked(cellIndex){
     this.filledCells ++;
     this.updateCurrentRows(cellIndex);

     if(this.isPlayerWin()){
       this.newGameAvailable = true;
       this.status = `"${this.currentPlayer}" won!`;
       this.status$.emit(this.status);
       this.newGameAvailable$.emit(this.newGameAvailable);
     } else {
       if(this.filledCells == 9){
          this.newGameAvailable = true;
          this.status = `Draw!`;
          this.status$.emit(this.status);
          this.newGameAvailable$.emit(this.newGameAvailable);
        } else {
          this.changePlayer();
        }
     }
  }

  changePlayer(){
    this.currentPlayer == "X" ? this.currentPlayer = "O" : this.currentPlayer = "X";
    this.newGameAvailable = true;
  }

  resetGame(){
    this.filledCells = 0;
    this.currentBoard.rows.forEach(function(row) {
      row.cells.forEach(function(cell) {
           cell.value = "";
      });
    });
    this.currentPlayer = "X"
    this.newGameAvailable = false;
    this.newGameAvailable$.emit(this.newGameAvailable);
  }

  updateCurrentRows(cellIndex){
    let that = this;
    this.currentBoard.rows.forEach(function(row) {
      row.cells.forEach(function(cell) {
        if(cell.index == cellIndex){
           cell.value = that.currentPlayer;
        }
      });
    });
  }

  isPlayerWin(){
    if(this.filledCells >= 5){
      if(this.currentBoard.rows[0].cells[0].value == this.currentPlayer &&
         this.currentBoard.rows[0].cells[1].value == this.currentPlayer &&
         this.currentBoard.rows[0].cells[2].value == this.currentPlayer ||
         this.currentBoard.rows[0].cells[0].value == this.currentPlayer &&
         this.currentBoard.rows[1].cells[0].value == this.currentPlayer &&
         this.currentBoard.rows[2].cells[0].value == this.currentPlayer ||
         this.currentBoard.rows[0].cells[0].value == this.currentPlayer &&
         this.currentBoard.rows[1].cells[1].value == this.currentPlayer &&
         this.currentBoard.rows[2].cells[2].value == this.currentPlayer ||
         this.currentBoard.rows[0].cells[1].value == this.currentPlayer &&
         this.currentBoard.rows[1].cells[1].value == this.currentPlayer &&
         this.currentBoard.rows[2].cells[1].value == this.currentPlayer ||
         this.currentBoard.rows[0].cells[2].value == this.currentPlayer &&
         this.currentBoard.rows[1].cells[2].value == this.currentPlayer &&
         this.currentBoard.rows[2].cells[2].value == this.currentPlayer ||
         this.currentBoard.rows[0].cells[2].value == this.currentPlayer &&
         this.currentBoard.rows[1].cells[1].value == this.currentPlayer &&
         this.currentBoard.rows[2].cells[0].value == this.currentPlayer ||
         this.currentBoard.rows[1].cells[0].value == this.currentPlayer &&
         this.currentBoard.rows[1].cells[1].value == this.currentPlayer &&
         this.currentBoard.rows[1].cells[2].value == this.currentPlayer ||
         this.currentBoard.rows[2].cells[0].value == this.currentPlayer &&
         this.currentBoard.rows[2].cells[1].value == this.currentPlayer &&
         this.currentBoard.rows[2].cells[2].value == this.currentPlayer){
           return true;
       };
    }
  }

}
