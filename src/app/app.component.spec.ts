import { TestBed, async } from '@angular/core/testing';

import { AppComponent } from './app.component';
import { TicTacBoardComponent } from './components/tic-tac-board/tic-tac-board.component';
import { TicTacCellComponent } from './components/tic-tac-board/tic-tac-cell/tic-tac-cell.component';
import { TicTacService } from './services/tic-tac.service';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        TicTacBoardComponent,
        TicTacCellComponent
      ],
       providers: [TicTacService]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  
});
